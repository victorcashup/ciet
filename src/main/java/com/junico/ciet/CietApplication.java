package com.junico.ciet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CietApplication {

	public static void main(String[] args) {
		SpringApplication.run(CietApplication.class, args);
	}
}
