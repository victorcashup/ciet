package com.junico.ciet;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.Gson;
import com.junico.ciet.models.comic.ComicDataWrapper;

import us.monoid.web.Resty;

public class MarvelApiSimpleTest {
	public static void xmain(String[] args) throws IOException {
		String publicKey = "b7ec7cb6d31cce4efd96bf3edf1fbc8f";
		String privateKey = "b6b36d2d5770c1dea740d6b9005e0aa67dc4b899";
		long timeStamp = System.currentTimeMillis();
		int limit = 5;

		String stringToHash = timeStamp + privateKey + publicKey;
		String hash = DigestUtils.md5Hex(stringToHash);

		String url = String.format(
				"http://gateway.marvel.com/v1/public/comics?ts=%d&apikey=%s&hash=%s&limit=%d&titleStartsWith=Spider",
				timeStamp, publicKey, hash, limit);
		System.out.println("URL=" + url);
		System.out.println("HASH=" + hash);
		String output = new Resty().text(url).toString();

		// ComicDataWrapper staff = new Gson().fromJson(new FileReader(new
		// File("a.json")), ComicDataWrapper.class);

		System.out.println(output);
	}

	public static void main(String[] args) throws Exception {
		ComicDataWrapper staff = new Gson().fromJson(new FileReader(new File("a.json")), ComicDataWrapper.class);
	}
}