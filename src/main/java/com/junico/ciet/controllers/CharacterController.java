package com.junico.ciet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.junico.ciet.models.character.Character;
import com.junico.ciet.services.CharacterService;

@RestController
@RequestMapping("/api/characters")
public class CharacterController {

	@Autowired
	public CharacterService characterService;

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Character get(@PathVariable("id") int id) {
		return characterService.get(id);
	}

	@RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
	public Character getByName(@PathVariable("name") String name) {
		return characterService.get(name);
	}

}
