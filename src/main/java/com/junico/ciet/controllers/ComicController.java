package com.junico.ciet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.junico.ciet.models.comic.Comic;
import com.junico.ciet.models.comic.ComicDataContainer;
import com.junico.ciet.services.ComicService;

@RestController
@RequestMapping("/api/comics")
public class ComicController {

	@Autowired
	public ComicService comicService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public DataTableJsonContainer search(@RequestParam(name = "search", required = false) String search,
			@RequestParam(name = "offset", defaultValue = "0") int offset,
			@RequestParam(name = "limit", defaultValue = "10") int limit) {
		ComicDataContainer result = comicService.search(search, offset, limit);
		DataTableJsonContainer json = new DataTableJsonContainer();
		json.total = result.total;
		json.rows = result.results;
		return json;
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Comic get(@PathVariable("id") int id) {
		return comicService.get(id);
	}
}
