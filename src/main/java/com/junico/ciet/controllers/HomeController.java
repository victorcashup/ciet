package com.junico.ciet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String home() {
		return "welcome";
	}

	@RequestMapping("/characters")
	public String characters() {
		return "characters";
	}

	@RequestMapping("/comics")
	public String comics() {
		return "comics";
	}
}
