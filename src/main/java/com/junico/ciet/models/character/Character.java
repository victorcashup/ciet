package com.junico.ciet.models.character;

import java.util.List;

import com.junico.ciet.models.MarvelImage;

public class Character {

	public int id;// (int, optional): The unique ID of the character resource.,
	public String name; // (string, optional): The name of the character.,
	public String description;// (string, optional): A short bio or description of the character.,
	public MarvelImage thumbnail;// (Image, optional): The representative image for this character.,
	public ComicList comics; // (ComicList, optional): A resource list containing comics which feature this
	// character.,

}

class ComicList {
	public List<ComicSummary> items;
}

class ComicSummary {
	public String resourceURI, name;
}