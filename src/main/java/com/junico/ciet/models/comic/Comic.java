package com.junico.ciet.models.comic;

import java.util.ArrayList;
import java.util.List;

import com.junico.ciet.models.MarvelImage;
import com.junico.ciet.models.MarvelTextObject;
import com.junico.ciet.models.MarvelURL;

public class Comic {
	//@formatter:off
	public int id;// (int, optional): The unique ID of the comic resource.,
	public String title;// (string, optional): The canonical title of the comic.,
	public String description;// (string, optional): The canonical title of the comic.,
	public int pageCount;// (int, optional): The number of story pages in the comic.,
	public String resourceURI;// (string, optional): The canonical URL identifier for this resource.,
	public MarvelImage thumbnail;// (Image, optional): The representative image for this comic.,
	
	public List<MarvelTextObject> textObjects; //(Array[TextObject], optional): A set of descriptive text blurbs for the comic.,
	public List<MarvelURL> urls; //(Array[Url], optional): A set of public web site URLs for the resource.,
	public List<MarvelImage> images;// (Array[Image], optional): A list of promotional images associated with this comic.,
	public CreatorList creators;// (CreatorList, optional): A resource list containing the creators associated with this comic.,
	public CharacterList characters;// (CreatorList, optional): A resource list containing the creators associated with this comic.,
	//@formatter:on

	public List<com.junico.ciet.models.character.Character> characterList = new ArrayList<>();

	public static class CreatorList {
		public List<CreatorSummary> items;
	}

	public static class CreatorSummary {
		public String name;// (string, optional): The full name of the character.,
		public String role;// (string, optional): The role of the creator in the parent entity.
	}

	public static class CharacterList {
		public List<CharacterSummary> items;
	}

	public static class CharacterSummary {
		public String name;// (string, optional): The full name of the character.,
		public String role;// (string, optional): The role of the creator in the parent entity.
	}
}
