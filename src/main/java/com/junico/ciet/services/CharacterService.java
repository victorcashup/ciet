package com.junico.ciet.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.junico.ciet.models.character.Character;
import com.junico.ciet.models.character.CharacterDataWrapper;

@Service
public class CharacterService extends MarvelAPIService {

	@Value("${characters.path}")
	private String path;

	public List<com.junico.ciet.models.character.Character> search(String name, int page, int size) {
		return null;
	}

	@Cacheable("characters")
	public com.junico.ciet.models.character.Character get(int id) {
		RestTemplate template = new RestTemplate();
		String url = path + "/" + id + getBaseParams();
		System.out.println("CALLING URL " + url);
		CharacterDataWrapper result = null;
		try {
			result = template.getForObject(url, CharacterDataWrapper.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == null || result.data.total == 0)
			return new Character();

		Character character = result.data.results.get(0);
		return character;
	}

	@Cacheable("charactersNames")
	public com.junico.ciet.models.character.Character get(String name) {
		String url = path + getBaseParams() + "&name=" + name + "&limit=1&offset=0";
		System.out.println("CALLING URL " + url);
		RestTemplate template = new RestTemplate();
		CharacterDataWrapper result = null;
		try {
			result = template.getForObject(url, CharacterDataWrapper.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == null || result.data.total == 0)
			return new Character();
		return result.data.results.get(0);
	}

}
