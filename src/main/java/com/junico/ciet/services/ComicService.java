package com.junico.ciet.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.junico.ciet.models.comic.Comic;
import com.junico.ciet.models.comic.Comic.CharacterSummary;
import com.junico.ciet.models.comic.ComicDataContainer;
import com.junico.ciet.models.comic.ComicDataWrapper;

@Service
public class ComicService extends MarvelAPIService {

	@Value("${comics.path}")
	private String path;

	@Autowired
	private CharacterService characterService;

	public ComicDataContainer search(String title, int offset, int limit) {
		if (StringUtils.isEmpty(title))
			return new ComicDataContainer();
		String url = path + getBaseParams() + "&titleStartsWith=" + title + "&limit=" + limit + "&offset=" + offset;
		System.out.println("CALLING URL " + url);
		RestTemplate template = new RestTemplate();
		ComicDataWrapper result = template.getForObject(url, ComicDataWrapper.class);
		return result.data;
	}

	@Cacheable("comics")
	public Comic get(int id) {
		RestTemplate template = new RestTemplate();
		String url = path + "/" + id + getBaseParams();
		System.out.println("CALLING URL " + url);

		ComicDataWrapper result = null;
		try {
			result = template.getForObject(url, ComicDataWrapper.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == null || result.data.total == 0)
			return new Comic();

		Comic comic = result.data.results.get(0);
		List<CharacterSummary> items = comic.characters.items;
		for (CharacterSummary cs : items) {
			comic.characterList.add(characterService.get(cs.name));
		}
		return comic;
	}

}
