package com.junico.ciet.services;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class MarvelAPIService {

	@Autowired
	private Environment env;

	public String getBaseParams() {
		String publicKey = env.getProperty("marvel.public.key");
		String privateKey = env.getProperty("marvel.private.key");
		long timeStamp = System.currentTimeMillis();

		String stringToHash = timeStamp + privateKey + publicKey;
		String hash = DigestUtils.md5Hex(stringToHash);

		String url = String.format("?ts=%d&apikey=%s&hash=%s", timeStamp, publicKey, hash);
//		System.out.println("URL=" + url);
//		System.out.println("HASH=" + hash);
//		System.out.println("pk=" + publicKey);
		return url;
	}
}
