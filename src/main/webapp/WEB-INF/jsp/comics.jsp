<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<title>Marvel Comics!</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="https://rawgit.com/wenzhixin/bootstrap-table/master/src/bootstrap-table.css">
<link href="/css/main.css" rel="stylesheet" />
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Marvel API Explorer </a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/characters">Search Characters</a></li>
					<li class="active"><a  href="/comics">Search Comics</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="starter-template">
			<h1>Search Comics</h1> 
		</div>
		<br />
		<div class="row">
			<div class="col-md-12">
				<table id="table"
		               data-toggle="table"
		               data-url="/api/comics/"
		               data-side-pagination="server"
		               data-pagination="true"
		               data-page-list="[10, 20, 50, 100]"
		               data-search="true"
		               data-mobileResponsive="true"
		               data-mobile-responsive="true"
		               >
		            <thead>
		            <tr>
		                <th width="30%" data-field="thumbnail" data-formatter="imageFormatter"></th>
		                <th width="80%" data-field="title" data-formatter="titleFormatter"></th>
		                <th width="5%" data-field="id" data-valign="middle" data-formatter="actionFormatter"></th>
		            </tr>
		            </thead>
		        </table>
	        </div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="comic-modal" tabindex="-1" >
	  <div class="modal-dialog" >
	    <div class="modal-content">
<!-- 	      <div class="modal-header"> -->
<!-- 	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!-- 	        <h4 class="modal-title comic-title" >Comic Title</h4> -->
<!-- 	      </div> -->
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<h2 class="text-center comic-title"></h2>
	        		<div class="text-center" id="thumbnail"></div>
	        		<div id="description"></div>
	        		<br />
	        		<div id="images"></div>
	        		<br />
	        		<div id="textObjects"></div>
	        		<br />
	        		<div id="characters"></div>
	        		<br />
	        		<div id="creators"></div>
	        		<br />
	        		<div id="urls"></div>
	        		<br />
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js" ></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://rawgit.com/wenzhixin/bootstrap-table/master/src/bootstrap-table.js"></script>

	<script type="text/javascript">
		function imageFormatter(value, row){
			var ret = "";
			ret += "<img data-id='"+row.id+"' onclick='show(event)' src='"+(value.path+'/portrait_xlarge.'+value.extension)+"' class='link'>";
			return ret;
		}
		
		function titleFormatter(value, row){
			var ret = "";
			ret += "<b>"+value+"</b>";
			ret += "<br/>"+(row.description || "");
			return ret;
		}
		
		function actionFormatter(value, row){
			var ret = "";
			ret += "<span style='margin:0 auto' data-id='"+row.id+"' onclick='show(event)' class='btn btn-success btn-lg'>BUY!</span>";
			return ret;
		}
		
		function show(e){
			var id = $(e.target).data('id');
			$.getJSON('/api/comics/'+id, function(json){
				console.log('comic json ', json);
				showModal(json);
			});
		}
		
		function showModal(json){
			$('.comic-title').text(json.title);
			$('#description').text(json.description);
			
			$("#thumbnail").html("<img src='"+(json.thumbnail.path+'/detail'+'.'+json.thumbnail.extension)+"'>")
			
// 			$("#images").children().remove();
// 			json.images.forEach(function(img){
// 				$("#images").append("<img src='"+(img.path+'/portrait_xlarge'+'.jpg')+"'>")
// 			});
			
			$("#textObjects").html('<h2>Mentions</h2>');
			json.textObjects.forEach(function(o){
				$("#textObjects").append(" - <span class='text-object'>"+o.text+"</span> <br />")
			});
			
			$("#creators").html("<h2>Creators</h2>");
			json.creators.items.forEach(function(o){
				$("#creators").append(o.name +" - <i> ("+o.role+") </i> <br />")
			});
			
			var detail, reader;
			json.urls.forEach(function(o){
				if(o.type=='detail') detail = o;
				if(o.type=='reader') reader = o;
			});
			var urls = $("#urls");
			urls.children().remove();
			if(detail) urls.append("<a target='_blank' href='"+detail.url+"'>See More... </a>");
			if(reader) urls.append("<a href='"+reader.url+"'> Open Online Reader</a>");
				
			var chars = $("#characters");
			chars.children().remove();
			chars.append('<h2>Characters</h2>')
			json.characterList.forEach(function(o){
				chars
					.append(
						"<div class='row'>"
							+"<div class='col-xs-4'>"
								+"<img src='"+(o.thumbnail.path+'/portrait_xlarge'+'.'+o.thumbnail.extension)+"'>"
							+"</div>"
							+"<div class='col-xs-8'>"
								+"<span class='char-name'>"+o.name+"</span>"
								+"<br /><span class='char-description'>"+o.description+"</span>"
							+"</div>"
						+"</div>"
					);
			});
			
			$("#comic-modal").modal('show');
		}
		
	</script>
</body>
</html>