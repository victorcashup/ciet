package com.junico.ciet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.junico.ciet.controllers.DataTableJsonContainer;
import com.junico.ciet.models.comic.Comic;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CietApplicationTests {

	public static final String server = "http://localhost:8080";

	@Test
	public void testSearch() {
		DataTableJsonContainer json = new RestTemplate().getForObject(
				server + "/api/comics/?search=Captain America&offset=0&limit=10", DataTableJsonContainer.class);
		assertFalse(json.rows.isEmpty());

		json = new RestTemplate().getForObject(server + "/api/comics/?search=Batman&offset=0&limit=10",
				DataTableJsonContainer.class);
		assertTrue(json.rows.isEmpty());

	}

	@Test
	public void testComic() {
		Comic json = new RestTemplate().getForObject(server + "/api/comics/38020", Comic.class);
		assertTrue(json.title.equals("SPIDER-MAN VS. VAMPIRES (2010) #3"));

		json = new RestTemplate().getForObject(server + "/api/comics/138020", Comic.class);
		assertNull(json.title);
	}

	@Test
	public void testCharacther() {
		com.junico.ciet.models.character.Character json = new RestTemplate()
				.getForObject(server + "/api/characters/1009220", com.junico.ciet.models.character.Character.class);
		assertEquals(("Captain America"), json.name);

		json = new RestTemplate().getForObject(server + "/api/characters/name/Superman",
				com.junico.ciet.models.character.Character.class);
		assertNull(json.name);
	}

}
